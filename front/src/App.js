import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import Main from "./containers/Main/Main";
import Login from "./containers/Register |  Login/Login";
import Register from "./containers/Register |  Login/Register";
import Layout from "./components/Layout/Layout";
import {Content} from "antd/es/layout/layout";
import CreateCocktail from "./containers/CreateCocktail/CreateCocktail";
import MyCocktails from "./containers/MyCocktails/MyCocktails";
import Cocktail from "./containers/Cocktail/Cocktail";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo}/>;
};


const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Layout>
      <Content style={{ padding: '0 50px', backgroundColor: '#fff' }}>
        <Switch>
          <ProtectedRoute
            path="/"
            exact
            component={Main}
            isAllowed={user}
            redirectTo="/login"
          />
          <ProtectedRoute
            path="/createCocktail"
            component={CreateCocktail}
            isAllowed={user}
            redirectTo="/login"
          />
          <ProtectedRoute
            path="/cocktail/:id"
            component={Cocktail}
            isAllowed={user}
            redirectTo="/login"
          />
          <ProtectedRoute
            path="/myCocktails"
            component={MyCocktails}
            isAllowed={user}
            redirectTo="/login"
          />
          <Route path="/register" component={Register}/>
          <Route path="/login" component={Login}/>
        </Switch>
      </Content>
    </Layout>
  );
};

export default App;
