import React from 'react';
import {Avatar, Button, Layout as AntLayout, Space} from "antd";
import {Link} from "react-router-dom";
import usersSlice from "../../store/slices/usersSlice";
import {useDispatch, useSelector} from "react-redux";
import { UserOutlined } from '@ant-design/icons';
import {apiURL} from "../../config";

const {Header} = AntLayout;

const {logoutRequest} = usersSlice.actions;

const Layout = ({children}) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const logout = () => {
    dispatch(logoutRequest());
  };

  return (
    <AntLayout>
      <Header style={{color: '#fff', display: 'flex', justifyContent: 'space-between', padding: '0 100px', marginBottom: 20}}>
        <div>
          <Link to='/' style={{textTransform: "uppercase", color: "#fff", fontWeight: 800}}>
            Cocktails
          </Link>
        </div>
        {user && <div>
          <Space size={20}>
            {user.avatar ? <Avatar src={apiURL + '/' + user.avatar} /> : <Avatar icon={<UserOutlined />} />}
            <span>Hello {user.displayName}</span>
            <Button type='primary' onClick={logout}>Logout</Button>
          </Space>
        </div>}
      </Header>
      {children}
    </AntLayout>
  );
};

export default Layout;
