import React from 'react';
import {useDispatch} from "react-redux";
import {facebookAppId} from "../../config";
import {Button} from "antd";
import {FacebookFilled} from "@ant-design/icons";
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import usersSlice from "../../store/slices/usersSlice";

const {facebookLoginRequest} = usersSlice.actions;

const FacebookLogin = ({loading}) => {
  const dispatch = useDispatch();

  const facebookResponse = response => {
    if (response.id) {
      dispatch(facebookLoginRequest(response));
    }
  };

  return (
    <FacebookLoginButton
      appId={facebookAppId}
      fields="name,email,picture"
      render={props => (
        <Button
          onClick={props.onClick}
          icon={<FacebookFilled />}
          type='primary'
          loading={loading}
        >Login with Facebook</Button>
      )}
      callback={facebookResponse}
    />
  );
};

export default FacebookLogin;
