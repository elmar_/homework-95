import React from 'react';
import {Button, Card as AntCard} from 'antd';
import noImage from '../../assets/images/no-image.jpg';
import {apiURL} from "../../config";
import {useSelector} from "react-redux";


const Card = ({title, image, onClick, user, id, onDelete, published, onPublish, realUser}) => {
  const loading = useSelector(state => state.cocktails.deleteLoading);

  let cardImage = noImage;

  if (image) {
    cardImage = apiURL + '/' + image;
  }
  const more = <Button onClick={onClick} type='primary'>more</Button>;



  return (
    <AntCard
      title={user._id === realUser ? (published ? 'Published' : 'Hidden') : null}
      hoverable
      style={{ width: 240 }}
      cover={<img alt={title} src={cardImage} />}
      bodyStyle={{backgroundColor: '#ececec'}}
      actions={user.role === 'admin' ? [
        <Button danger onClick={() => onDelete(id)} loading={loading}>delete</Button>,
        <Button onClick={() => onPublish(id)}>{published ? 'Hide' : 'Publish'}</Button>,
        more
      ] : [more]}
    >
      {title}
    </AntCard>
  );
};

export default Card;
