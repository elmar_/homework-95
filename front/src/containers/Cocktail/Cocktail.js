import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import cocktailSlice from "../../store/slices/cocktailsSlice";
import {Button, Col, Divider, List, Row, Space, Typography} from "antd";
import noImage from "../../assets/images/no-image.jpg";
import {apiURL} from "../../config";
import cocktailsSlice from "../../store/slices/cocktailsSlice";
import {historyPush} from "../../store/actions/historyActions";

const {fetchOneRequest} = cocktailSlice.actions;
const {deleteRequest, publishRequest} = cocktailsSlice.actions;

const Cocktail = ({match}) => {
  const dispatch = useDispatch();
  const cocktail = useSelector(state => state.cocktails.cocktail);
  const user = useSelector(state => state.users.user);
  const id = match.params.id;

  let cardImage = noImage;

  if (cocktail.image) {
    cardImage = apiURL + '/' + cocktail.image;
  }

  const deleteCocktail = () => {
    dispatch(deleteRequest(id));
    dispatch(historyPush('/'));
  };

  const publishCocktail = () => {
    dispatch(publishRequest(id));
  };

  useEffect(() => {
    dispatch(fetchOneRequest(id));
  }, [dispatch, id]);

  return (
    <div>
      {user.role === 'admin' ?
        <>
          <Divider />
          <Space size={20}>
            <Button onClick={publishCocktail}>{cocktail.published ? 'Hide' : 'Publish'}</Button>
            <Button danger onClick={deleteCocktail}>Delete</Button>
          </Space>
        </> :
        null
      }
      <Divider />
      <Row>
        <Col style={{border: '1px solid black', marginRight: 30}}>
          <img src={cardImage} alt={cocktail.title} width={400} height='auto' />
        </Col>
        <Col span={9}>
          <Typography.Title level={3}>{cocktail.title}</Typography.Title>
          <List
            header={<div>Ingredients</div>}
            bordered
            dataSource={cocktail.ingredients}
            renderItem={item => (
              <List.Item>
                <Typography.Text mark>{item.title}</Typography.Text> - {item.amount}
              </List.Item>
            )}
          />
        </Col>
      </Row>
      <Row style={{marginTop: 20}}>
        <Col>
          <Typography.Title level={5}>Recipe</Typography.Title>
          <Typography>{cocktail.recipe}</Typography>
        </Col>
      </Row>
    </div>
  );
};

export default Cocktail;
