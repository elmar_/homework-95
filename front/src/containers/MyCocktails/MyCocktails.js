import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import cocktailsSlice from "../../store/slices/cocktailsSlice";
import Card from "../../components/Card/Card";
import {Empty, Space, Spin} from "antd";
import {historyPush} from "../../store/actions/historyActions";

const {fetchRequest, deleteRequest, publishRequest} = cocktailsSlice.actions;

const MyCocktails = () => {
  const dispatch = useDispatch();
  const cocktails = useSelector(state => state.cocktails.cocktails);
  const user = useSelector(state => state.users.user);
  const loading = useSelector(state => state.cocktails.fetchLoading);

  useEffect(() => {
    dispatch(fetchRequest(user._id))
  }, [dispatch, user._id]);

  const onClick = id => {
    dispatch(historyPush('/cocktail/' + id));
  };

  const deleteCocktail = id => {
    dispatch(deleteRequest(id));
  };

  const publishCocktail = id => {
    dispatch(publishRequest(id));
  };

  let content = (
    <Space direction="horizontal" size={20} wrap={true}>
      {cocktails.map(item => (
        <Card title={item.title} image={item.image} key={item._id} id={item._id} onClick={() => onClick(item._id)}
              user={user} onDelete={deleteCocktail} published={item.published} onPublish={publishCocktail}
              realUser={item.user}/>
      ))}
    </Space>
  );

  if  (cocktails.length === 0) {
    content = <Empty description="No cocktails" />
  }

  if (loading) {
    content = <Spin />
  }


  return (
    <div>
      {content}
    </div>
  );
};

export default MyCocktails;
