import React, {useEffect} from 'react';
import {Button, Col, Empty, Row, Space, Spin} from "antd";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import cocktailsSlice from "../../store/slices/cocktailsSlice";
import Card from "../../components/Card/Card";

const {fetchRequest, deleteRequest, publishRequest} = cocktailsSlice.actions;

const Main = ({history}) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const cocktails = useSelector(state => state.cocktails.cocktails);
  const loading = useSelector(state => state.cocktails.fetchLoading);

  useEffect(() => {
    dispatch(fetchRequest());
  }, [dispatch]);

  const onClick = id => {
    history.push('/cocktail/' + id);
  };

  const publishCocktail = id => {
    dispatch(publishRequest(id));
  };

  const deleteCocktail = id => {
    dispatch(deleteRequest(id));
  };

  let content = (
    <Space direction="horizontal" size={20} wrap={true}>
      {cocktails.map(item => (
        <Card title={item.title} image={item.image} key={item._id} id={item._id} onClick={() => onClick(item._id)}
              user={user} onDelete={deleteCocktail} published={item.published} onPublish={publishCocktail}
              realUser={item.user}/>
      ))}
    </Space>
  );

  if  (cocktails.length === 0) {
    content = <Empty description="No cocktails" />
  }

  if (loading) {
    content = <Spin />
  }



  return (
    <>
      {user ? <Row justify="space-between" style={{marginBottom: 30}}>
        <Col span={6} style={{textAlign: 'center'}}>
          <Link component={Button} to="/myCocktails">My cocktails</Link>
        </Col>

        <Col span={6} style={{textAlign: 'center'}}>
          <Link component={Button} to="/createCocktail">Create cocktail</Link>
        </Col>
      </Row> : null}

      {content}

    </>
  );
};

export default Main;
