import React from 'react';
import {Alert, Button, Col, Form, Input, Row} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import usersSlice from "../../store/slices/usersSlice";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";


const {loginRequest} = usersSlice.actions;

const Login = () => {
  const loading = useSelector(state => state.users.loginLoading);
  const error = useSelector(state => state.users.loginError);
  const dispatch = useDispatch();

  const onFinish = data => {
    dispatch(loginRequest(data));
  };


  return (
    <div style={{paddingTop: 200}}>
      <Form
        onFinish={onFinish}
      >
        {<Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item>
              {error && <Alert
                message="Error Text"
                description={error.message || error.global || "Error, try again"}
                type="error"
              />}
            </Form.Item>
          </Col>
        </Row>}

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Please input your email!',
                  type: "email"
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9}>
            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading} style={{marginRight: 20}}>
                Login
              </Button>
              <FacebookLogin loading={loading}/>
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item>
              <Link to='/register'>Dont have account?</Link>
            </Form.Item>
          </Col>
        </Row>

      </Form>
    </div>
  );
};

export default Login;
