import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, Col, Form, Input, Row, Upload} from "antd";
import {Link} from "react-router-dom";
import usersSlice from "../../store/slices/usersSlice";
import {UploadOutlined} from "@ant-design/icons";

const {registerRequest} = usersSlice.actions;

const Register = () => {
  const loading = useSelector(state => state.users.registerLoading);
  const error = useSelector(state => state.users.registerError);
  const dispatch = useDispatch();

  const onFinish = data => {
    if (data.avatar){
      const newData = {...data, avatar: data.avatar[0].originFileObj}
      dispatch(registerRequest(newData));
    } else {
      data.avatar = null;
      dispatch(registerRequest(data));
    }


  };

  const normFile = (e) => {

    if (Array.isArray(e)) {
      return e;
    }

    return e && e.fileList;
  };


  return (
    <div style={{paddingTop: 200}}>
      <Form
        onFinish={onFinish}
      >
        {<Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item>
              {error && <Alert
                message="Error Text"
                description={error.message || error.global || "Error, try again"}
                type="error"
              />}
            </Form.Item>
          </Col>
        </Row>}

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Please input your email!',
                  type: "email"
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Display name"
              name="displayName"
              rules={[
                {
                  required: true,
                  message: 'Please input your display name!',
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              name="avatar"
              label="Avatar"
              valuePropName="fileList"
              getValueFromEvent={normFile}
            >
              <Upload name="logo" listType="picture" maxCount={1} beforeUpload={() => false}>
                <Button icon={<UploadOutlined />}>Click to upload</Button>
              </Upload>
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9}>
            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading} style={{marginRight: 20}}>
                Register
              </Button>
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item>
              <Link to='/login'>Do you already have account?</Link>
            </Form.Item>
          </Col>
        </Row>

      </Form>
    </div>
  );
};

export default Register;
