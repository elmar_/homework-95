import React from 'react';
import {Alert, Button, Col, Form, Input, Row, Space, Typography, Upload} from "antd";
import {MinusCircleOutlined, PlusOutlined, UploadOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import cocktailsSlice from "../../store/slices/cocktailsSlice";

const {Title} = Typography;

const {createRequest} = cocktailsSlice.actions;

const CreateCocktail = () => {
  const dispatch = useDispatch();
  const error = useSelector(state => state.cocktails.postError);
  const loading = useSelector(state => state.cocktails.postLoading);

  const onFinish = data => {
    if (data.image?.length){
      const newData = {...data, image: data.image[0].originFileObj}
      dispatch(createRequest(newData));
      console.log(newData);
    } else {
      data.image = null;
      dispatch(createRequest(data));
      console.log(data);
    }
  };

  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }

    return e && e.fileList;
  };


  return (
    <div>
      <Title level={3}>Add new cocktail</Title>

      <Form
        onFinish={onFinish}
      >
        {<Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item>
              {error && <Alert
                message="Error Text"
                description={error.message || error.global || "Error, try again"}
                type="error"
              />}
            </Form.Item>
          </Col>
        </Row>}

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Title"
              name="title"
              rules={[
                {
                  required: true,
                  message: 'Please input title!',
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.List name="ingredients">
              {(fields, { add, remove }) => (
                <>
                  {fields.map(({ key, name, fieldKey}) => (
                    <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                      <Form.Item
                        label="Ingredient title"
                        name={[name, 'title']}
                        fieldKey={[fieldKey, 'first']}
                        rules={[{ required: true, message: 'Missing first name' }]}
                      >
                        <Input />
                      </Form.Item>
                      <Form.Item
                        name={[name, 'amount']}
                        label="Amount"
                        fieldKey={[fieldKey, 'last']}
                        rules={[{ required: true, message: 'Missing last name' }]}
                      >
                        <Input/>
                      </Form.Item>
                      <MinusCircleOutlined onClick={() => remove(name)} />
                    </Space>
                  ))}
                  <Form.Item>
                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                      Add field
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Recipe"
              name="recipe"
              rules={[
                {
                  required: true,
                  message: 'Please input your display name!',
                },
              ]}
            >
              <Input.TextArea />
            </Form.Item>
          </Col>
        </Row>


        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              name="image"
              label="Image"
              valuePropName="fileList"
              getValueFromEvent={normFile}
            >
              <Upload name="logo" listType="picture" maxCount={1} beforeUpload={() => false}>
                <Button icon={<UploadOutlined />}>Click to upload</Button>
              </Upload>
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9}>
            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading} style={{marginRight: 20}}>
                Create
              </Button>
            </Form.Item>
          </Col>
        </Row>

      </Form>
    </div>
  );
};

export default CreateCocktail;
