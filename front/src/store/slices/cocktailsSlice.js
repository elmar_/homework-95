const {createSlice} = require("@reduxjs/toolkit");

const initialState = {
  cocktails: [],
  cocktail: {},
  fetchLoading: false,
  fetchError: null,
  postLoading: false,
  postError: null,
  fetchOneLoading: false,
  fetchOneError: null,
  deleteLoading: false,
  publishLoading: false
};

const name = 'cocktails';

const cocktailSlice = createSlice({
  name,
  initialState,
  reducers: {
    createRequest: state => {
      state.postLoading = true;
    },
    createSuccess: state => {
      state.postLoading = false;
      state.postError = null;
    },
    createError: (state, {payload: error}) => {
      state.postError = error;
      state.postLoading = false;
    },
    fetchRequest: state => {
      state.fetchLoading = true;
    },
    fetchSuccess: (state, {payload}) => {
      state.fetchLoading = false;
      state.fetchError = null;
      state.cocktails = payload;
    },
    fetchError: (state, {payload: error}) => {
      state.fetchLoading = false;
      state.fetchError = error;
    },
    fetchOneRequest: state => {
      state.fetchOneLoading = true;
    },
    fetchOneSuccess: (state, {payload: cocktail}) => {
      state.cocktail = cocktail;
      state.fetchOneLoading = false;
      state.fetchOneError = null;
    },
    fetchOneError: (state, {payload: error}) => {
      state.fetchOneLoading = false;
      state.fetchOneError = error;
    },
    deleteRequest: state => {
      state.deleteLoading = true;
    },
    deleteSuccess: (state, {payload: id}) => {
      state.deleteLoading = false;
      const index = state.cocktails.findIndex(item => item._id === id);
      state.cocktails.splice(index, 1);
      if (state.cocktail._id === id) {
        state.cocktail = null;
      }
    },
    deleteError: state => {
      state.deleteLoading = false;
    },
    publishRequest: state => {
      state.publishLoading = true;
    },
    publishSuccess: (state, {payload: id}) => {
      state.publishLoading = false;
      const index = state.cocktails.findIndex(item => item._id === id);
      state.cocktails[index].published = !state.cocktails[index]?.published;
      if (state.cocktail._id === id) {
        state.cocktail.published = !state.cocktail.published;
      }
    },
    publishError: state => {
      state.publishLoading = false;
    }
  }
});

export default cocktailSlice;
