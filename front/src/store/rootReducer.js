import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import cocktailSlice from "./slices/cocktailsSlice";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  cocktails: cocktailSlice.reducer
});

export default rootReducer;
