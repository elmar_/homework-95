import cocktailSlice from "../slices/cocktailsSlice";
import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {notificationError, notificationSuccess} from "../../components/Notification/Notification";
import {historyPush} from "../actions/historyActions";


const {
  createError,
  createRequest,
  createSuccess,
  fetchError,
  fetchRequest,
  fetchSuccess,
  fetchOneError,
  fetchOneSuccess,
  fetchOneRequest,
  deleteSuccess,
  deleteError,
  deleteRequest,
  publishError,
  publishRequest,
  publishSuccess
} = cocktailSlice.actions;

function* createCocktail({payload: data}) {
  try {
    data.ingredients = JSON.stringify(data.ingredients);
    const newData = new FormData();
    Object.keys(data).forEach(key => {
      newData.append(key, data[key]);
    });
    yield axiosApi.post('/cocktails', newData);
    yield put(createSuccess());
    notificationSuccess('Cocktail created');
    yield put(historyPush('/'));
  } catch (e) {
    console.error(e);
    notificationError('cant fetch create cocktail');
    yield put(createError(e));
  }
}

function* fetchCocktails({payload: user}) {
  try {
    const response = yield axiosApi.get('/cocktails', {params: {user}});
    yield put(fetchSuccess(response.data));
  } catch (e) {
    console.error(e);
    notificationError('cant fetch cocktails');
    yield put(fetchError(e));
  }
}

function* fetchOne({payload: id}) {
  try{
    const response = yield axiosApi.get('/cocktails/' + id);
    yield put(fetchOneSuccess(response.data));
  } catch (e) {
    console.error(e);
    notificationError('cant fetch cocktail');
    yield put(fetchOneError(e));
  }
}

function* deleteCocktail({payload: id}) {
  try{
    yield axiosApi.delete('/cocktails/' + id);
    yield put(deleteSuccess(id));
    notificationSuccess('Cocktail deleted');
  } catch (e) {
    console.error(e);
    notificationError('Cant delete cocktail');
    yield put(deleteError());
  }
}

function* publishCocktail({payload: id}) {
  try{
    yield axiosApi.put('/cocktails/' + id);
    yield put(publishSuccess(id));
    notificationSuccess('Cocktail corrected');
  } catch (e) {
    console.error(e);
    notificationError('Cant publish cocktail');
    yield put(publishError());
  }
}

const cocktailsSagas = [
  takeEvery(createRequest, createCocktail),
  takeEvery(fetchRequest, fetchCocktails),
  takeEvery(fetchOneRequest, fetchOne),
  takeEvery(deleteRequest, deleteCocktail),
  takeEvery(publishRequest, publishCocktail)
];

export default cocktailsSagas;
