import {put, takeEvery} from "redux-saga/effects";
import usersSlice from "../slices/usersSlice";
import {historyPush} from "../actions/historyActions";
import axiosApi from "../../axiosApi";
import {notificationError, notificationSuccess} from "../../components/Notification/Notification";

const {
  facebookLoginRequest,
  loginFailure,
  loginRequest,
  loginSuccess, logoutRequest, logoutSuccess,
  registerFailure,
  registerRequest,
  registerSuccess,
} = usersSlice.actions;


export function* registerUser({payload: userData}) {
  try {
    const data = new FormData();
    Object.keys(userData).forEach(key => {
      data.append(key, userData[key]);
    });
    const response = yield axiosApi.post('/users', data);
    yield put(registerSuccess(response.data));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(registerFailure(error.response.data));
  }
}

export function* loginUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    notificationSuccess('Login successful');
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* facebookLogin({payload: data}) {
  try {
    const response = yield axiosApi.post('/users/facebookLogin', data);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    notificationSuccess('Login success');
  } catch (error) {
    yield put(loginFailure(error.response.data));
    notificationError('Login failed');
  }
}

export function* logout() {
  try {
    yield axiosApi.delete('/users/sessions');
    yield put(logoutSuccess());
    yield put(historyPush('/'));
  } catch (e) {
    notificationError('Logout failed');
  }
}


const usersSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(facebookLoginRequest, facebookLogin),
  takeEvery(logoutRequest, logout),
];

export default usersSagas;
