import {all} from "redux-saga/effects";
import historySagas from "./sagas/historySagas";
import history from "../history";
import usersSagas from "./sagas/usersSagas";
import cocktailsSagas from "./sagas/cocltailsSagas";


function* rootSaga() {
  yield all([
    ...historySagas(history),
    ...usersSagas,
    ...cocktailsSagas,
  ]);
}

export default rootSaga;
