const mongoose = require('mongoose');

const IngredientSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  amount: {
    type: String,
    required: true
  }
});

const RateSchema = new mongoose.Schema({
  rate: {
    type: Number,
    required: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
});


const CocktailSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  image: String,
  recipe: {
    type: String,
    required: true
  },
  published: {
    type: Boolean,
    default: false
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  ingredients: [IngredientSchema],
  rates: [RateSchema]
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;
