const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const {nanoid} = require('nanoid');
const Cocktail = require('./models/Cocktail');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@test.com',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'user',
  }, {
    email: 'admin@test.com',
    password: '123',
    token: nanoid(),
    role: 'admin',
    displayName: 'admin'
  });


  await Cocktail.create({
    title: 'Коктейлей c кукурузными хлопьями',
    recipe: 'Залейте 1,5 ст. подслащенных кукурузных хлопьев (сухой завтрак) 1,5 ст. горячего молока на 30 мин. Процедите и заморозьте в лотке для льда. Смешайте в блендере замороженные кубики молока с 500 мл. ванильного мороженого и 1 ст. кукурузных хлопьев. Посыпьте хлопьями.',
    published: true,
    user: user,
    image: 'fixtures/fixture_1.jpg',
    ingredients: [{
      title: 'Кукурузные хлопья',
      amount: '1,5 ст',
    }, {
      title: 'Сахар',
      amount: '2 столовой лошки',
    }]
  });




  await mongoose.connection.close();
};

run().catch(e => console.error(e));
