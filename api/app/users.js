const express = require('express');
const User = require('../models/User');
const config = require('../config');
const axios = require("axios");
const auth = require("../middleware/auth");
const {downloadAvatar} = require("../utils");
const {nanoid} = require("nanoid");
const upload = require('../multer').avatar;

const router = express.Router();

router.post('/', upload.single('avatar'), async (req, res) => {
  try {
    const user = new User({
      email: req.body.email,
      password: req.body.password,
      displayName: req.body.displayName,
      avatar: req.file ? req.file.filename : null
    });

    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({email: req.body.email})
    .populate([{path: 'friends', select: 'displayName'}, {path: 'shared', select: 'displayName'}]);

  if (!user) {
    return res.status(401).send({message: 'Credentials are wrong'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({message: 'Credentials are wrong'});
  }

  user.generateToken();
  await user.save();

  return res.send({message: 'Email and password correct!', user});
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};

  if (!token) return res.send(success);

  const user = await User.findOne({token});

  if (!user) return res.send(success);

  user.generateToken();

  await user.save();

  return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
  const inputToken = req.body.accessToken;

  const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);

    if (response.data.data.error) {
      return res.status(401).send({message: "Facebook token incorrect"});
    }

    if (response.data.data['user_id'] !== req.body.id) {
      return res.status(401).send({global: 'User id incorrect'});
    }

    let user = await User.findOne({email: req.body.email});

    if (!user) {
      user = await User.findOne({facebookId: req.body.id});
    }

    const pictureUrl = req.body.picture.data.url;
    const avatarFilename = await downloadAvatar(pictureUrl);

    if (!user) {
      user = new User({
        email: req.body.email || nanoid(),
        password: nanoid(),
        facebookId: req.body.id,
        displayName: req.body.name
      });
    }

    user.avatar = avatarFilename;

    user.generateToken();
    await user.save();


    res.send({message: "Success", user});
  } catch (e) {
    return res.status(401).send({global: 'Facebook token incorrect'});
  }
});

router.put('/addFriend', auth, async (req, res) => {
  try {
    const user = req.user;
    const email = req.body;
    const friend = await User.findOne(email);

    if (!friend) {
      return res.status(401).send({message: 'no such user'});
    }

    await friend.updateOne({friends: [...friend.friends, user]});

    await user.updateOne({shared: [...user.shared, friend]});

    const toSend = await User.findOne({_id: user._id})
      .populate([{path: 'friends', select: 'displayName'}, {path: 'shared', select: 'displayName'}]);


    return res.send(toSend);
  } catch (e) {
    console.log(e)
    return res.status(400).send({message: 'error in put user'});
  }
});


module.exports = router;
